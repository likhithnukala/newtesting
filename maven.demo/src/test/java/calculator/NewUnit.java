package calculator;
import org.junit.BeforeClass;
import org.junit.Test;

import calculator.NewTesting;
import junit.framework.TestCase;



public class NewUnit extends TestCase {
	
		private static NewTesting calculator;
		
		@BeforeClass
		
		public static void setup() {
			calculator = new NewTesting();
			
		}
		@Test
		public void testAddition() {
			assertEquals(7, calculator.addition(2, 5));
		}
		@Test
		public void testSubtraction() {
			assertEquals(2, calculator.subtraction(5, 3));
		}
		@Test
		public void testmultiplication() {
		assertEquals(12, calculator.multiplication(4, 3));
		}
		@Test
		public void testsquare() {
			assertEquals(9, calculator.square(3));
		}
		@Test
		public void testfactorial() {
			assertEquals(6, calculator.factorial(3));
		}
		@Test
		public void testreminder() {
			assertEquals(1, calculator.reminder(7, 3));
		}
}


